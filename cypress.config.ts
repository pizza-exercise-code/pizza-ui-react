import { defineConfig } from 'cypress';

export default defineConfig({
  component: {
    setupNodeEvents() {},
    specPattern: 'src/**/*.test.{js,ts,jsx,tsx}'
  },
  e2e: {
    baseUrl: 'http://localhost:3000'
  }
});
