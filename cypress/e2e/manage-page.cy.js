describe('Manage Page', () => {

  beforeEach(() => {
    cy.visit('/manage');
    cy.get('#manage-tab-0').click();
  });

  it('should render the home page successfully', () => {
    cy.get('.css-1qncr2t > .MuiTypography-h4').contains("Manage the Pizzas and Toppings");
    cy.get('.MuiTypography-body2').contains("Use the tabs to navigate between pizzas and toppings");
  });

  describe('Add Pizza Form', () => {
    it('should create a new pizza when the form to add pizza is render', () => {
      cy.get('.MuiButton-root').click();
      cy.get('#dialog-pizza').contains("Create Pizza");
      cy.get('.btn-pz-modal-secondary').should("exist");
      cy.get('.btn-pz-modal-primary').should("exist");

      cy.get('#name').type("CY PIZZA TEST");
      cy.get('#description').type("DESCRIPTION");
      cy.get('#price').type(2);
      cy.get('.btn-pz-modal-primary').click();
      cy.wait(2000)
    });

    it('should not create a pizza with the form is empty', () => {
      cy.get('.MuiButton-root').click();
      cy.get('.btn-pz-modal-primary').click();

      cy.get('#name-helper-text').contains("The pizza name is requiered");
      cy.get('#description-helper-text').contains("The pizza description is requiered");
      cy.get('#price-helper-text').contains("The price is requiered");
    });
  });

  describe('Update Pizza Form', () => {
    it('should update the a pizza created when form has valid data', () => {
      cy.get(':nth-child(1) > :nth-child(5) > [data-testid="ModeEditOutlineOutlinedIcon"]').click();
      cy.get('#dialog-pizza').contains("Update Pizza");
      cy.get('.btn-pz-modal-secondary').should("exist");
      cy.get('.btn-pz-modal-primary').should("exist");
      cy.get('#name').type("CY PIZZA TEST UPDATE");
      cy.get('#description').type("DESCRIPTION UPDATE");
      cy.get('#price').type(10);
      cy.get('.btn-pz-modal-primary').click();

      cy.wait(2000);
    });
  });

  describe('Add topping to pizza', () => {
    it('should add topping to pizza', () => {
      cy.get('#manage-tab-1').click();
      cy.get('.MuiTabs-flexContainer').next().click();
      cy.get('.MuiButton-root').click();

      cy.get('#name').type("CYPRESS TOPPING");
      cy.get('#description').type("THIS IS A CYPRESS TOPPING");
      cy.get('.btn-pz-modal-primary').click();

      cy.get('#manage-tab-0').click();

      cy.get(':nth-child(1) > :nth-child(5) > [data-testid="ModeEditOutlineOutlinedIcon"]').click();
      cy.get('.MuiAutocomplete-root > .MuiFormControl-root > .MuiOutlinedInput-root').type("CYPRESS TOPPING");
      cy.get('#select-topping-option-0').click();
      cy.get('.btn-pz-modal-primary').click();
      cy.wait(2000);

      cy.get(':nth-child(1) > :nth-child(5) > [data-testid="ModeEditOutlineOutlinedIcon"]').click();
      cy.get('.MuiChip-label').contains("CYPRESS TOPPING");
      cy.get('.btn-pz-modal-secondary').click();

      cy.get('#manage-tab-1').click();
      cy.get(':nth-child(1) > :nth-child(4) > [data-testid="DeleteOutlinedIcon"]').click();
      cy.get('.btn-pz-modal-primary').click();
    });
  });

  describe('Delete Pizza', () => {
    it('should delete a new pizza when user clicks the delete icon', () => {
      cy.get(':nth-child(1) > :nth-child(5) > [data-testid="DeleteOutlinedIcon"]').click();
      cy.get('#dialog-pizza').contains("Delete Pizza");
      cy.get('.btn-pz-modal-primary').click();
    });
  });
});
