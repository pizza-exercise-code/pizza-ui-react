describe('Store page', () => {

  beforeEach(() => {
    cy.visit('/fakeUrl')
  });

  it('should render the not found page successfully', () => {
    cy.get('.MuiTypography-h2').contains("Oops not found page");
    cy.get('.css-8c5xtf-MuiGrid-root > .MuiGrid-container > .MuiGrid-root > .MuiTypography-body1').contains("You are looking for a non-existing page, please press the button below to return to the home page.");
  });

  it('should redirect to home page on click BACK TO HOME button', () => {
    cy.get('.MuiButton-root').click();

    cy.get('.MuiTypography-h2').contains("The best Pizza around the World");
    cy.get('.MuiTypography-body1').contains("Search, select and manage new pizzas that is on our online pizza menu");
  });
})
