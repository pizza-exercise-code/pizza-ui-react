describe('Home Page', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('should render the home page successfully', () => {
    cy.get('.MuiTypography-h2').contains("The best Pizza around the World");
    cy.get('.MuiTypography-body1').contains("Search, select and manage new pizzas that is on our online pizza menu");
  });

  it('should redirect to Store page on click ORDER NOW button', () => {
    cy.get('.MuiButton-root').click();
    cy.get('.MuiTypography-h2').contains("Find Your favorite pizza");
    cy.get('.css-8c5xtf-MuiGrid-root > .MuiGrid-container > .MuiGrid-root > .MuiTypography-body1').contains("Visit our store and select the pizza that your prefer!");
  });
})
