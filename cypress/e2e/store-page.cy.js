describe('Store page', () => {

  beforeEach(() => {
    cy.visit('/store')
  });

  it('should render the store page successfully', () => {
    cy.get('.MuiTypography-h2').contains("Find Your favorite pizza");
    cy.get('.css-8c5xtf-MuiGrid-root > .MuiGrid-container > .MuiGrid-root > .MuiTypography-body1').contains("Visit our store and select the pizza that your prefer!");
  });

  it('should redirect to pizza information on click SEE TOPPINGS button', () => {
    cy.get(':nth-child(1) > .wrapper__card > .css-e2ivvl-MuiGrid-root > .MuiButton-root').click();

    cy.get('.MuiTypography-h3').should("exist")
    cy.get(':nth-child(1) > .MuiTypography-body1').should("exist")
    cy.get(':nth-child(1) > .MuiTypography-h6').should("exist")
    cy.get('.pizza-image').should("exist")
  });
})
