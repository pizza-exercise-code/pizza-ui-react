# PIZZA UI - REACT

This project is a code exercise development with React library v18 and Node v16.13.

For styles, it uses MUI (Material UI) library which is based on components for React.

This project also includes automation e2e tests created and execute with Cypress.

## Pizza API

To run the project, before you have to download and run the PIZZA-API project, you can download and setup from the next [Link](https://gitlab.com/pizza-exercise-code/pizza-api).

## Execute the Project
You have to install all the package dependencies using NPM (`npm install`) or YARN (`yarn`). 

Then you can use the command `npm run start` or `yarn start` to execute the project, after this the project will open on [http://localhost:3000](http://localhost:3000)

## Open Cypress UI
To execute the tests you have to verify that all the dependencies of Cypress were installed inside your node_modules directory.

Then you can run the next command (`yarn run cypress open`) or (`npx cypress open`) to open the user interface that has to run the e2e tests.

*Keep in mind that before running the tests Pizza-api and Pizza-ui-react must be running.*
