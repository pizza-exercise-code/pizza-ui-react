import IToppingModel from '../ToppingModel';

export default interface IToppingState {
  loading: boolean;
  sucess: boolean;
  toppings: IToppingModel[];
}
