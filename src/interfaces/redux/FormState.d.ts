import IFormData from '../Form';

export default interface IFormState {
  formConfiguration: IFormData;
}
