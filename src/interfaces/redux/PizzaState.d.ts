import IPizzaModel from '../PizzaModel';

export default interface IPizzaState {
  loading: boolean;
  sucess: boolean;
  pizzas: IPizzaModel[];
}
