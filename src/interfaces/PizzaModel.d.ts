export default interface IPizzaModel {
  id: string;
  name: string;
  description: string;
  price: number;
  imageUrl: string;
}
