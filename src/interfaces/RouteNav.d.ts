export default interface IRouteNavigation {
  name: string;
  url: string;
}
