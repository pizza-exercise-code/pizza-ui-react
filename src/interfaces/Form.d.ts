export default interface IFormData {
  method?: string;
  formTitle: string;
  btnLabelClose: string;
  btnLabelSubmit: string;
  message?: string;
}
