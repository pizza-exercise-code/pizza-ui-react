export default interface IToppingModel {
  id: string;
  name: string;
  description: string;
}
