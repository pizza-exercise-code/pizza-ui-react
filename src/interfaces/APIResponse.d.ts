export default interface IAPIResponse<T> {
  status: number;
  data: T;
  message: string;
}
