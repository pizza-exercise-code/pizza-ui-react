import IPizzaModel from './PizzaModel';
import IToppingModel from './ToppingModel';

export default interface IPizzaFlavor {
  pizza: IPizzaModel;
  toppings: IToppingModel[];
}
