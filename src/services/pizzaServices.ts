import axios from 'axios';
import IPizzaModel from '../interfaces/PizzaModel';
import IToppingModel from '../interfaces/ToppingModel';
import ApplicationConfiguration from '../utils/enviroments';

const BASE_PATH = ApplicationConfiguration.PIZZA_API;

async function getPizzas() {
  let pizzaResult;
  const result = await axios.get(`${BASE_PATH}/api/pizzas`);
  if (result.status === 200) pizzaResult = result.data;
  else pizzaResult = null;
  return pizzaResult;
}

async function getPizzaFlavorById(id: string) {
  let pizzaFlavor;
  const result = await axios.get(`${BASE_PATH}/api/pizzas/${id}/toppings`);
  if (result.status === 200) {
    pizzaFlavor = result.data;
  }
  return pizzaFlavor;
}

async function postPizza(pizza: IPizzaModel) {
  let pizzaModel;
  const result = await axios.post(`${BASE_PATH}/api/pizzas`, pizza);
  if (result.status === 200) {
    pizzaModel = result.data;
  }
  return pizzaModel;
}

async function putPizza(pizzaId: string, pizza: IPizzaModel) {
  let pizzaModel;
  const result = await axios.put(`${BASE_PATH}/api/pizzas/${pizzaId}`, pizza);
  if (result.status === 200) {
    pizzaModel = result.data;
  }
  return pizzaModel;
}

async function addToppingsToPizza(pizzaId: string, toppings: IToppingModel[]) {
  let pizzaToppings;
  const result = await axios.post(
    `${BASE_PATH}/api/pizzas/${pizzaId}/add-toppings-to-pizza`,
    toppings
  );
  if (result.status === 200) {
    pizzaToppings = result.data;
  }
  return pizzaToppings;
}

async function deletePizza(pizzaId: String) {
  let pizzaToppings;
  const result = await axios.delete(`${BASE_PATH}/api/pizzas/${pizzaId}`);
  if (result.status === 200) {
    pizzaToppings = result.data;
  }
  return pizzaToppings;
}

const pizzaServices = {
  getPizzas,
  getPizzaFlavorById,
  postPizza,
  addToppingsToPizza,
  putPizza,
  deletePizza
};

export default pizzaServices;
