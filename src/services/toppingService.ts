import axios from 'axios';
import IToppingModel from '../interfaces/ToppingModel';
import ApplicationConfiguration from '../utils/enviroments';

const BASE_PATH = ApplicationConfiguration.PIZZA_API;

async function getToppings() {
  const result = await axios.get(`${BASE_PATH}/api/toppings`);
  return result.data;
}

async function postTopping(toppingModel: IToppingModel) {
  const result = await axios.post(`${BASE_PATH}/api/toppings`, toppingModel);
  return result.data;
}

async function updateTopping(toppingId: String, toppingModel: IToppingModel) {
  const result = await axios.put(`${BASE_PATH}/api/toppings/${toppingId}`, toppingModel);
  return result.data;
}

async function deleteTopping(toppingId: String) {
  const result = await axios.delete(`${BASE_PATH}/api/toppings/${toppingId}`);
  return result.data;
}

const toppingService = {
  getToppings,
  postTopping,
  updateTopping,
  deleteTopping
};

export default toppingService;
