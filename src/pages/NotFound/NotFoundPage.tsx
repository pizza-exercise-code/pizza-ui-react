import { Container } from '@mui/system';
import { Component, ReactNode } from 'react';
import HeroComponent from '../../components/genericComponents/heroComponent/HeroComponent';
import { hero } from './NotFoundPageMock';

class NotFound extends Component {
  render(): ReactNode {
    return (
      <Container>
        <HeroComponent {...hero} />
      </Container>
    );
  }
}

export default NotFound;
