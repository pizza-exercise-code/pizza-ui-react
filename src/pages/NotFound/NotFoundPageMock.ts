export const hero = {
  title: 'Oops not found page',
  description:
    'You are looking for a non-existing page, please press the button below to return to the home page.',
  componentConfiguration: {
    size: 450
  },
  button: {
    buttonLabel: 'Back to Home',
    buttonHref: '/'
  }
};
