export const hero = {
  title: 'The best Pizza around the World',
  description: 'Search, select and manage new pizzas that is on our online pizza menu',
  imageUrl: 'https://cdn.pixabay.com/photo/2017/12/05/20/10/pizza-3000285__340.png',
  button: {
    buttonLabel: 'Order Now',
    buttonHref: '/store'
  },
  componentConfiguration: {
    size: 650
  }
};
