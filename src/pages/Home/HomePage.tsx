import { Component, ReactNode } from 'react';
import HeroComponent from '../../components/genericComponents/heroComponent/HeroComponent';
import { hero } from './HeroMock';

class Home extends Component {
  render(): ReactNode {
    return <HeroComponent {...hero}></HeroComponent>;
  }
}

export default Home;
