import { Container } from '@mui/system';
import { Component, ReactNode } from 'react';
import HeroComponent from '../../components/genericComponents/heroComponent/HeroComponent';
import PizzaCardComponent from '../../components/storeComponents/pizzaComponent/PizzaCardComponent';
import { hero } from './StoreMock';

class Store extends Component {
  render(): ReactNode {
    return (
      <Container>
        <HeroComponent {...hero} />
        <PizzaCardComponent></PizzaCardComponent>
      </Container>
    );
  }
}

export default Store;
