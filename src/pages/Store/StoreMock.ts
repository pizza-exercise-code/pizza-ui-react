export const hero = {
  title: 'Find Your favorite pizza',
  description: 'Visit our store and select the pizza that your prefer!',
  componentConfiguration: {
    size: 300
  }
};
