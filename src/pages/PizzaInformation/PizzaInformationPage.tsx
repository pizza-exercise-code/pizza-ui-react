import { Container } from '@mui/system';
import { Component, ReactNode } from 'react';
import PizzaInformationComponent from '../../components/pizzaInformationComponent/pizzaComponent/PizzaInformationComponent';

class PizzaInformation extends Component {
  render(): ReactNode {
    return (
      <Container>
        <PizzaInformationComponent></PizzaInformationComponent>
      </Container>
    );
  }
}

export default PizzaInformation;
