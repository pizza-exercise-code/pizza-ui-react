export const hero = {
  title: 'Manage pizzas and toppings',
  description: 'Use the tab options to manage toppings or pizzas',
  componentConfiguration: {
    size: 300
  }
};
