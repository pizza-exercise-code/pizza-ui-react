import { Container } from '@mui/system';
import { Component, ReactNode } from 'react';
import TabComponent from '../../components/manageComponents/tabComponent/TabComponent';

class Manage extends Component {
  render(): ReactNode {
    return (
      <Container>
        <TabComponent></TabComponent>
      </Container>
    );
  }
}

export default Manage;
