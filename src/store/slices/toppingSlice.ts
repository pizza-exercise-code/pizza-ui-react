import { createSlice } from '@reduxjs/toolkit';
import IToppingModel from '../../interfaces/ToppingModel';
import toppingActions from '../actions/toppingActions';

export const toppingSlice = createSlice({
  name: 'toppings',
  initialState: {
    loading: false,
    sucess: true,
    toppings: [] as IToppingModel[]
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(toppingActions.getToppings.pending, (state) => {
      state.loading = true;
      state.sucess = true;
    });
    builder.addCase(toppingActions.getToppings.fulfilled, (state, action) => {
      state.loading = false;
      state.sucess = true;
      state.toppings = action.payload;
    });
    builder.addCase(toppingActions.getToppings.rejected, (state) => {
      state.sucess = false;
    });
    builder.addCase(toppingActions.createToppings.fulfilled, (state, action) => {
      state.toppings.unshift(action.payload);
    });
    builder.addCase(toppingActions.deleteToppings.fulfilled, (state, action) => {
      if (action.payload.wasDeleted) {
        state.toppings = state.toppings.filter(
          (topping: IToppingModel) => topping.id !== action.payload.toppingId
        );
      }
    });
    builder.addCase(toppingActions.updateToppings.fulfilled, (state, action) => {
      const index = state.toppings.findIndex(
        (topping: IToppingModel) => topping.id === action.payload.id
      );
      state.toppings[index] = action.payload;
    });
  }
});

export default toppingSlice.reducer;
