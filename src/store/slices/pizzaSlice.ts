import { createSlice } from '@reduxjs/toolkit';
import IPizzaModel from '../../interfaces/PizzaModel';
import pizzaActions from '../actions/pizzaActions';

export const pizzaSlice = createSlice({
  name: 'pizzas',
  initialState: {
    loading: false,
    sucess: true,
    pizzas: [] as IPizzaModel[]
  },
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(pizzaActions.getPizzas.pending, (state) => {
      state.loading = true;
      state.sucess = true;
    });
    builder.addCase(pizzaActions.getPizzas.fulfilled, (state, action) => {
      state.loading = false;
      state.sucess = true;
      state.pizzas = action.payload;
    });
    builder.addCase(pizzaActions.getPizzas.rejected, (state) => {
      state.sucess = false;
    });
    builder.addCase(pizzaActions.createPizza.fulfilled, (state, action) => {
      state.pizzas.unshift(action.payload);
    });
    builder.addCase(pizzaActions.deletePizza.fulfilled, (state, action) => {
      state.pizzas = state.pizzas.filter(
        (pizza: IPizzaModel) => pizza.id !== action.payload.pizzaId
      );
    });
    builder.addCase(pizzaActions.updatePizza.fulfilled, (state, action) => {
      const index = state.pizzas.findIndex((pizza: IPizzaModel) => pizza.id === action.payload.id);
      state.pizzas[index] = action.payload;
    });
  }
});

export default pizzaSlice.reducer;
