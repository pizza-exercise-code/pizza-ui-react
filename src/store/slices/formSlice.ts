import { createSlice } from '@reduxjs/toolkit';

export const formSlice = createSlice({
  name: 'pizzas',
  initialState: {
    formConfiguration: {
      formTitle: '',
      method: '',
      btnLabelClose: 'Close',
      btnLabelSubmit: ''
    }
  },
  reducers: {
    postForm: (state, action) => {
      state.formConfiguration.btnLabelSubmit = `Create ${action.payload}`;
      state.formConfiguration.method = `POST`;
      state.formConfiguration.formTitle = `Create ${action.payload}`;
    },
    putForm: (state, action) => {
      state.formConfiguration.btnLabelSubmit = `Update ${action.payload}`;
      state.formConfiguration.method = `PUT`;
      state.formConfiguration.formTitle = `Update ${action.payload}`;
    }
  }
});

export const { postForm, putForm } = formSlice.actions;
export default formSlice.reducer;
