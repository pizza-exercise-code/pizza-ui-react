import { configureStore } from '@reduxjs/toolkit';
import pizzaReducer from './slices/pizzaSlice';
import formReducer from './slices/formSlice';
import toppingReducer from './slices/toppingSlice';

export const store = configureStore({
  reducer: {
    pizzas: pizzaReducer,
    toppings: toppingReducer,
    form: formReducer
  }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
