import { createAsyncThunk } from '@reduxjs/toolkit';
import IAPIResponse from '../../interfaces/APIResponse';
import IPizzaModel from '../../interfaces/PizzaModel';
import IToppingModel from '../../interfaces/ToppingModel';
import pizzaService from '../../services/pizzaServices';

export interface IPizzaData {
  toppings: IToppingModel[];
  pizzaModel: IPizzaModel;
}

const getPizzas = createAsyncThunk('pizza/getPizzas', async () => {
  const result = await pizzaService.getPizzas();
  return result.data;
});

const createPizza = createAsyncThunk('pizza/createPizza', async (pizzaData: IPizzaData) => {
  const { pizzaModel, toppings } = pizzaData;
  const result: IAPIResponse<IPizzaModel> = await pizzaService.postPizza(pizzaModel);
  if (toppings.length > 0) {
    await pizzaService.addToppingsToPizza(result.data.id, toppings);
  }
  return result.data;
});

const deletePizza = createAsyncThunk('pizza/deletePizza', async (pizzaId: String) => {
  const resultApi = await pizzaService.deletePizza(pizzaId);
  const result = {
    wasDeleted: resultApi.data,
    pizzaId: pizzaId
  };
  return result;
});

const updatePizza = createAsyncThunk('pizza/updatePizza', async (pizzaData: IPizzaData) => {
  const { pizzaModel, toppings } = pizzaData;
  const result: IAPIResponse<IPizzaModel> = await pizzaService.putPizza(pizzaModel.id, pizzaModel);
  await pizzaService.addToppingsToPizza(result.data.id, toppings);
  return result.data;
});

const pizzaActions = {
  getPizzas,
  createPizza,
  deletePizza,
  updatePizza
};

export default pizzaActions;
