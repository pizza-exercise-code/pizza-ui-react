import { createAsyncThunk } from '@reduxjs/toolkit';
import IAPIResponse from '../../interfaces/APIResponse';
import IToppingModel from '../../interfaces/ToppingModel';
import toppingService from '../../services/toppingService';

const getToppings = createAsyncThunk('topping/getPizzas', async () => {
  const result = await toppingService.getToppings();
  return result.data;
});

const createToppings = createAsyncThunk(
  'topping/createPizza',
  async (toppingModel: IToppingModel) => {
    const result: IAPIResponse<IToppingModel> = await toppingService.postTopping(toppingModel);
    return result.data;
  }
);

const deleteToppings = createAsyncThunk('topping/deletePizza', async (toppingId: String) => {
  const resultApi = await toppingService.deleteTopping(toppingId);
  const result = {
    wasDeleted: resultApi.data,
    toppingId: toppingId
  };
  return result;
});

const updateToppings = createAsyncThunk('topping/updatePizza', async (topping: IToppingModel) => {
  const result: IAPIResponse<IToppingModel> = await toppingService.updateTopping(
    topping.id,
    topping
  );
  return result.data;
});

const toppingActions = {
  getToppings,
  createToppings,
  deleteToppings,
  updateToppings
};

export default toppingActions;
