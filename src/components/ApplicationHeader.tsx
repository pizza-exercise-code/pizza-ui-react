import { Container, Toolbar } from '@mui/material';
import { Link } from 'react-router-dom';
import IRouteNavigation from '../interfaces/RouteNav';

const routes: IRouteNavigation[] = [
  {
    name: 'Home',
    url: '/'
  },
  {
    name: 'Store',
    url: '/store'
  },
  {
    name: 'Manage Pizza',
    url: '/manage'
  }
];

const ApplicationHeader = () => {
  return (
    <Toolbar className="pizza-navbar">
      <Container className="pizza-navbar__item-container">
        {routes.map((routes) => (
          <Link
            className="pizza-navbar__item-link"
            color="inherit"
            key={routes.name}
            to={routes.url}>
            {routes.name}
          </Link>
        ))}
      </Container>
    </Toolbar>
  );
};

export default ApplicationHeader;
