import { CircularProgress, Grid } from '@mui/material';
import { FC, useEffect } from 'react';
import CardComponent from '../../genericComponents/cardComponent/CardComponent';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../store/store';
import IPizzaState from '../../../interfaces/redux/PizzaState';
import IPizzaModel from '../../../interfaces/PizzaModel';
import pizzaActions from '../../../store/actions/pizzaActions';

const PizzaCardComponent: FC = () => {
  const pizzaState = useSelector<RootState, IPizzaState>((state) => state.pizzas);
  const dispatch = useDispatch<AppDispatch>();
  useEffect(() => {
    dispatch(pizzaActions.getPizzas());
  }, [dispatch]);

  return (
    <Grid container spacing={4}>
      {pizzaState.loading ? (
        <Grid container>
          <CircularProgress size={80} className="loading-icon" />
        </Grid>
      ) : (
        pizzaState.pizzas.map((pizza: IPizzaModel) => (
          <Grid xs={12} sm={6} md={4} item key={pizza.id}>
            <CardComponent {...pizza}></CardComponent>
          </Grid>
        ))
      )}
    </Grid>
  );
};

export default PizzaCardComponent;
