import { Button, CircularProgress, Grid, Typography } from '@mui/material';
import { Container } from '@mui/system';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import IToppingModel from '../../../interfaces/ToppingModel';
import pizzaServices from '../../../services/pizzaServices';
import './PizzaInformationComponent.scss';

const PizzaInformationComponent = () => {
  const [pizzaFlavor, setPizzaFlavor] = useState<any>(null);

  useEffect(() => {
    getPizzaFlavor();
  }, []);

  const getPizzaFlavor = async () => {
    let pizzaFlavorId = window.location.pathname.split('/')[2];
    const response = await pizzaServices.getPizzaFlavorById(pizzaFlavorId);
    setPizzaFlavor(response.data);
  };

  return (
    <Container sx={{ minHeight: '80vh', display: 'flex' }}>
      {pizzaFlavor ? (
        <Grid container spacing={3} sx={{ m: 'auto' }}>
          <Grid item={true} xs={12} md={6} sx={{ display: 'flex' }}>
            <img
              className="pizza-image"
              src={pizzaFlavor.pizza.imageUrl}
              alt="image"
              onError={({ currentTarget }) => {
                currentTarget.onerror = null;
                currentTarget.src =
                  'https://eventovirtual.co/wp-content/themes/appon/assets/images/no-image/No-Image-Found-400x264.png';
              }}
            />
          </Grid>
          <Grid item={true} xs={12} md={6}>
            <Grid>
              <Typography variant="h3" className="text-bold" sx={{ marginBottom: 1 }}>
                Pizza {pizzaFlavor.pizza.name}
              </Typography>
              <Typography variant="body1" sx={{ marginBottom: 1 }}>
                {pizzaFlavor.pizza.description}
              </Typography>
              <Typography variant="h6" className="text-bold text-color__secondary">
                ${pizzaFlavor.pizza.price}
              </Typography>
            </Grid>
            <Grid>
              {pizzaFlavor.toppings.length > 0 ? (
                <Typography variant="h6" className="text-bold" sx={{ marginBottom: 2 }}>
                  Topping List
                </Typography>
              ) : (
                <></>
              )}
              {pizzaFlavor.toppings.map((topping: IToppingModel) => (
                <div key={topping.id}>
                  <Typography
                    variant="body1"
                    className="text-bold text-color__secondary"
                    sx={{ marginBottom: 1 }}>
                    {topping.name}
                  </Typography>
                  <Typography variant="body1">{topping.description}</Typography>
                </div>
              ))}
            </Grid>
            <Link to={'/store'}>
              <Button className="btn btn-pz-primary" sx={{ marginTop: 2 }}>
                Back to Menu
              </Button>
            </Link>
          </Grid>
        </Grid>
      ) : (
        <>
          <Grid container>
            <CircularProgress size={80} className="loading-icon" />
          </Grid>
        </>
      )}
    </Container>
  );
};

export default PizzaInformationComponent;
