import {
  Autocomplete,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  useMediaQuery,
  useTheme
} from '@mui/material';
import { forwardRef, SyntheticEvent, useEffect, useImperativeHandle, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { AppDispatch, RootState } from '../../../store/store';
import IPizzaModel from '../../../interfaces/PizzaModel';
import IToppingModel from '../../../interfaces/ToppingModel';
import IToppingState from '../../../interfaces/redux/ToppingState';
import IFormState from '../../../interfaces/redux/FormState';
import pizzaActions, { IPizzaData } from '../../../store/actions/pizzaActions';

interface DialogPizzaComponentProps {
  isDisplayed: boolean;
  pizzaToppingSelected: IToppingModel[];
  closeModal: Function;
}

const DialogPizzaComponent = forwardRef((props: DialogPizzaComponentProps, ref: any) => {
  const formState = useSelector<RootState, IFormState>((state) => state.form);
  const toppingsState = useSelector<RootState, IToppingState>((state) => state.toppings);
  const dispatch = useDispatch<AppDispatch>();
  const { pizzaToppingSelected, isDisplayed } = props;
  const theme = useTheme();
  const [pizzaId, setPizzaId] = useState<string>('');
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [open, setOpen] = useState<boolean>(false);
  const [toppingSelected, setToppingSelected] = useState<IToppingModel[]>([]);
  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors }
  } = useForm<IPizzaModel>();

  useEffect(() => {
    setOpen(isDisplayed);
  });

  useImperativeHandle(ref, () => ({
    setValuesToForm(pizza: IPizzaModel, toppingsSelected: IToppingModel[]) {
      setPizzaId(pizza.id);
      setValue('name', pizza.name);
      setValue('description', pizza.description);
      setValue('imageUrl', pizza.imageUrl);
      setValue('price', pizza.price);
      setToppingSelected(toppingsSelected);
    }
  }));

  const handleClose = () => {
    props.closeModal();
    reset();
  };

  const onSubmit = async (pizzaForm: IPizzaModel) => {
    const pizzaData: IPizzaData = {
      toppings: toppingSelected,
      pizzaModel: buildPizzaModel(pizzaForm)
    };
    if (formState.formConfiguration.method === 'PUT') dispatch(pizzaActions.updatePizza(pizzaData));
    else dispatch(pizzaActions.createPizza(pizzaData));
    handleClose();
  };

  const buildPizzaModel = (pizzaForm: IPizzaModel) => {
    let pizzaData = {
      ...pizzaForm,
      id: formState.formConfiguration.method === 'PUT' ? pizzaId : pizzaForm.id,
      price: parseInt(pizzaForm.price.toString())
    };
    return pizzaData;
  };

  const handleChange = (event: SyntheticEvent, topping: IToppingModel[]) => {
    setToppingSelected(topping);
  };

  const getToppingsSelected = (toppingSelected: IToppingModel[]) => {
    let finalListResult: IToppingModel[] = [];
    if (toppingsState.toppings.length > 0 && formState.formConfiguration.method === 'PUT') {
      toppingSelected.forEach((topping: IToppingModel) => {
        let index = toppingsState.toppings.findIndex((e: IToppingModel) => {
          return e.name === topping.name;
        });
        finalListResult.push(toppingsState.toppings[index]);
      });
    }
    return finalListResult;
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="dialog-pizza">
      <DialogTitle id="dialog-pizza" variant="h5" className="dialog-form__title text-bold">
        {formState.formConfiguration.formTitle}
      </DialogTitle>
      <DialogContent>
        <form className="dialog-form" onSubmit={handleSubmit(onSubmit)}>
          <TextField
            className="dialog-form__input"
            size="small"
            id="name"
            label={'Pizza Name *'}
            {...register('name', {
              required: 'The pizza name is requiered',
              maxLength: {
                value: 50,
                message: 'The name of the pizza is too long, use one less than 50 characters.'
              }
            })}
            error={errors?.name ? true : false}
            helperText={errors?.name ? errors.name.message : null}
          />
          <TextField
            className="dialog-form__input"
            size="small"
            id="description"
            label={'Pizza Description *'}
            {...register('description', {
              required: 'The pizza description is requiered',
              maxLength: {
                value: 150,
                message:
                  'The description of the pizza is too long, use one less than 150 characters.'
              }
            })}
            error={errors?.description ? true : false}
            helperText={errors?.description ? errors.description.message : null}
          />
          <TextField
            className="dialog-form__input"
            size="small"
            id="imageUrl"
            label={'Pizza ImageUrl'}
            {...register('imageUrl', {
              maxLength: {
                value: 250,
                message: 'The image URL is too long, use one less than 250 characters.'
              }
            })}
            error={errors?.imageUrl ? true : false}
            helperText={errors?.imageUrl ? errors.imageUrl.message : null}
          />
          <TextField
            className="dialog-form__input"
            label={'Pizza Price *'}
            size="small"
            id="price"
            type="number"
            InputLabelProps={{
              shrink: true
            }}
            {...register('price', {
              required: 'The price is requiered',
              min: {
                value: 1,
                message: 'The pizza price can not be less that 1.'
              }
            })}
            error={errors?.price ? true : false}
            helperText={errors?.price ? errors.price.message : null}
          />
          <Autocomplete
            multiple
            onChange={handleChange}
            id="select-topping"
            className="dialog-form__input"
            options={toppingsState.toppings}
            getOptionLabel={(option: IToppingModel) => option.name}
            defaultValue={getToppingsSelected(pizzaToppingSelected)}
            renderInput={(params) => (
              <TextField {...params} label="Select Toppings" placeholder="Toppings" />
            )}
          />
          <DialogActions className="dialog-action-section">
            <Button className="btn btn-pz-modal-secondary" onClick={() => handleClose()}>
              {formState.formConfiguration.btnLabelClose}
            </Button>
            <Button className="btn btn-pz-modal-primary" type="submit">
              {formState.formConfiguration.btnLabelSubmit}
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
});

export default DialogPizzaComponent;
