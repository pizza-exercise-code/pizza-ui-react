import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  useMediaQuery,
  useTheme
} from '@mui/material';
import { FC, useEffect, useState } from 'react';
import IFormData from '../../../interfaces/Form';

export interface DialogDeleteConfirmationProps {
  closeModal: () => void;
  confirmModal: () => void;
  isDisplayed: boolean;
  formConfiguration: IFormData;
}

const DialogDeleteConfirmation: FC<DialogDeleteConfirmationProps> = (props) => {
  const { formConfiguration, isDisplayed } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(isDisplayed);
  });

  const handleClose = () => {
    props.closeModal();
  };

  const handleConfirmModal = () => {
    props.confirmModal();
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="dialog-pizza">
      <DialogTitle id="dialog-pizza" variant="h5" className="dialog-form__title text-bold">
        {formConfiguration.formTitle}
      </DialogTitle>
      <DialogContent>
        <Typography variant="body1" sx={{ padding: 1 }}>
          {formConfiguration.message}
        </Typography>
      </DialogContent>
      <DialogActions sx={{ paddingRight: 4, paddingBottom: 4 }}>
        <Button className="btn btn-pz-modal-secondary" onClick={() => handleClose()}>
          {formConfiguration.btnLabelClose}
        </Button>
        <Button className="btn btn-pz-modal-primary" onClick={() => handleConfirmModal()}>
          {formConfiguration.btnLabelSubmit}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DialogDeleteConfirmation;
