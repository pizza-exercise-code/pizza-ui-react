import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  useMediaQuery,
  useTheme
} from '@mui/material';
import { forwardRef, useEffect, useImperativeHandle, useState } from 'react';
import { useForm } from 'react-hook-form';
import IToppingModel from '../../../interfaces/ToppingModel';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../store/store';
import IFormState from '../../../interfaces/redux/FormState';
import IFormData from '../../../interfaces/Form';
import toppingActions from '../../../store/actions/toppingActions';

interface DialogAddToppingComponentProps {
  isDisplayed: boolean;
  closeModal: () => void;
  formConfiguration: IFormData;
}

const DialogAddToppingComponent = forwardRef((props: DialogAddToppingComponentProps, ref: any) => {
  const dispatch = useDispatch<AppDispatch>();
  const formState = useSelector<RootState, IFormState>((state) => state.form);
  const { isDisplayed } = props;
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [open, setOpen] = useState(isDisplayed);
  const [toppingId, setToppingId] = useState('');
  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors }
  } = useForm<IToppingModel>();

  useImperativeHandle(ref, () => ({
    setValuesToForm(toppingModel: IToppingModel) {
      setToppingId(toppingModel.id);
      setValue('name', toppingModel.name);
      setValue('description', toppingModel.description);
    }
  }));

  useEffect(() => {
    setOpen(isDisplayed);
  });

  const handleClose = () => {
    props.closeModal();
    reset();
  };

  const onSubmit = async (toppingModel: IToppingModel) => {
    if (formState.formConfiguration.method === 'PUT') {
      toppingModel.id = toppingId;
      dispatch(toppingActions.updateToppings(toppingModel));
    } else {
      dispatch(toppingActions.createToppings(toppingModel));
    }
    reset();
    handleClose();
  };

  return (
    <Dialog
      fullScreen={fullScreen}
      open={open}
      onClose={handleClose}
      aria-labelledby="dialog-pizza">
      <DialogTitle id="dialog-pizza" variant="h5" className="dialog-form__title text-bold">
        {formState.formConfiguration.formTitle}
      </DialogTitle>
      <DialogContent>
        <form className="dialog-form" onSubmit={handleSubmit(onSubmit)}>
          <TextField
            className="dialog-form__input"
            size="small"
            id="name"
            label={'Topping Name *'}
            {...register('name', {
              required: 'The topping name is requiered',
              maxLength: {
                value: 50,
                message: 'The name of the topping is too long, use one less than 50 characters.'
              }
            })}
            error={errors?.name ? true : false}
            helperText={errors?.name ? errors.name.message : null}
          />
          <TextField
            className="dialog-form__input"
            size="small"
            id="description"
            label={'Topping Description *'}
            {...register('description', {
              required: 'The topping description is requiered',
              maxLength: {
                value: 150,
                message:
                  'The description of the topping is too long, use one less than 150 characters.'
              }
            })}
            error={errors?.description ? true : false}
            helperText={errors?.description ? errors.description.message : null}
          />
          <DialogActions className="dialog-action-section">
            <Button className="btn btn-pz-modal-secondary" onClick={() => handleClose()}>
              {formState.formConfiguration.btnLabelClose}
            </Button>
            <Button className="btn btn-pz-modal-primary" type="submit" autoFocus>
              {formState.formConfiguration.btnLabelSubmit}
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
});

export default DialogAddToppingComponent;
