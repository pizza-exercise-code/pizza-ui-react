import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import { FC, ReactNode, SyntheticEvent, useEffect, useState } from 'react';
import TableToppingComponent from '../tableComponent/TableToppingComponent';
import TablePizzaComponent from '../tableComponent/TablePizzaComponent';
import { Tabs, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../../../store/store';
import toppingActions from '../../../store/actions/toppingActions';
import pizzaActions from '../../../store/actions/pizzaActions';

interface TabPanelProps {
  children?: ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}>
      {value === index && <Box sx={{ marginTop: 2 }}>{children}</Box>}
    </div>
  );
}

const TabComponent: FC = () => {
  const [value, setValue] = useState<number>(0);
  const dispatch = useDispatch<AppDispatch>();
  const handleChange = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    dispatch(toppingActions.getToppings());
    dispatch(pizzaActions.getPizzas());
  }, [dispatch]);

  function a11yProps(index: number) {
    return {
      id: `manage-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`
    };
  }

  return (
    <>
      <Box sx={{ marginTop: 8, marginBottom: 4 }}>
        <Typography variant={'h4'} align="center" className="text-bold">
          Manage the Pizzas and Toppings
        </Typography>
        <Typography variant={'body2'} align="center">
          Use the tabs to navigate between pizzas and toppings
        </Typography>
      </Box>
      <Box sx={{ marginTop: 2 }}>
        <Tabs value={value} onChange={handleChange} aria-label="tabs">
          <Tab label="Pizzas" {...a11yProps(0)} />
          <Tab label="Toppings" {...a11yProps(1)} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <TablePizzaComponent />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <TableToppingComponent />
        </TabPanel>
      </Box>
    </>
  );
};

export default TabComponent;
