import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';
import { FC, useRef, useState } from 'react';
import IToppingModel from '../../../interfaces/ToppingModel';
import NoDataFoundMessage from '../../genericComponents/loadingComponent/NoDataFoundMessage';
import DialogAddToppingComponent from '../dialogComponent/DialogToppingComponent';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../../store/store';
import { postForm, putForm } from '../../../store/slices/formSlice';
import IFormData from '../../../interfaces/Form';
import DialogDeleteConfirmation from '../dialogComponent/DialogDeleteConfirmation';
import IToppingState from '../../../interfaces/redux/ToppingState';
import toppingActions from '../../../store/actions/toppingActions';

let deleteFormData: IFormData = {
  formTitle: 'Delete Topping',
  btnLabelClose: 'Close',
  btnLabelSubmit: 'Delete Topping',
  message: ''
};

const TableToppingComponent: FC = () => {
  const toppingState = useSelector<RootState, IToppingState>((state) => state.toppings);
  const dispatch = useDispatch<AppDispatch>();

  const [openToppingDialogModal, setOpenToppingDialogModal] = useState<boolean>(false);
  const [openDeleteDialogModal, setDialogDeleteModal] = useState(false);
  const [formConfiguration, setFormConfiguration] = useState<IFormData>(deleteFormData);
  const [toppingId, setToppingId] = useState('');
  const childRef: any = useRef(null);

  const handleCloseDialogModal = () => {
    setOpenToppingDialogModal(false);
    setDialogDeleteModal(false);
  };

  const diplayInputForm = () => {
    setOpenToppingDialogModal(true);
  };

  const handleDeleteTopping = async () => {
    dispatch(toppingActions.deleteToppings(toppingId));
    setDialogDeleteModal(false);
  };

  const handleClickOpenToppingAddForm = () => {
    dispatch(postForm('Topping'));
    diplayInputForm();
  };

  const handleClickOpenToppingUpadteForm = (topping: IToppingModel) => {
    dispatch(putForm('Topping'));
    childRef.current.setValuesToForm(topping);
    diplayInputForm();
  };

  const handleClickDeleteDialogConfirmation = (topping: IToppingModel) => {
    deleteFormData.message = `Are you sure that you want to delete the Topping ${topping.name} from the store?`;
    setFormConfiguration(deleteFormData);
    setDialogDeleteModal(true);
    setToppingId(topping.id);
  };

  return (
    <>
      <Typography variant="h4" className="text-bold">
        Topping List
        <Button
          className="btn btn-pz-primary"
          onClick={handleClickOpenToppingAddForm}
          sx={{ marginLeft: 2 }}>
          Add new Topping
        </Button>
      </Typography>
      <TableContainer sx={{ marginTop: 2 }}>
        <Table sx={{ minWidth: 750 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className="text-bold" sx={{ width: 50, paddingTop: 1, paddingBottom: 1 }}>
                Nro
              </TableCell>
              <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} className="text-bold">
                Name
              </TableCell>
              <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} className="text-bold">
                Description
              </TableCell>
              <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} className="text-bold">
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          {toppingState.toppings.length > 0 ? (
            <TableBody>
              {toppingState.toppings.map((topping: IToppingModel, index: number) => (
                <TableRow
                  key={index.toString()}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} component="th" scope="row">
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    {topping.name}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    {topping.description}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    <ModeEditOutlineOutlinedIcon
                      sx={{ marginLeft: 1, marginRight: 1 }}
                      className="icon-edit"
                      onClick={() => handleClickOpenToppingUpadteForm(topping)}
                    />
                    <DeleteOutlinedIcon
                      sx={{ marginLeft: 1, marginRight: 1 }}
                      onClick={() => handleClickDeleteDialogConfirmation(topping)}
                      className="icon-delete"
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : (
            <NoDataFoundMessage columnsSize={5} message="No toppings were found" />
          )}
        </Table>
      </TableContainer>
      <div>
        <DialogAddToppingComponent
          closeModal={() => handleCloseDialogModal()}
          isDisplayed={openToppingDialogModal}
          ref={childRef}
          formConfiguration={formConfiguration}></DialogAddToppingComponent>
        <DialogDeleteConfirmation
          closeModal={() => handleCloseDialogModal()}
          confirmModal={() => handleDeleteTopping()}
          isDisplayed={openDeleteDialogModal}
          formConfiguration={formConfiguration}></DialogDeleteConfirmation>
      </div>
    </>
  );
};

export default TableToppingComponent;
