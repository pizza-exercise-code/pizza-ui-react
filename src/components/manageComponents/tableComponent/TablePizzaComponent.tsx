import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';
import { FC, useRef, useState } from 'react';
import IPizzaModel from '../../../interfaces/PizzaModel';
import pizzaServices from '../../../services/pizzaServices';
import NoDataFoundMessage from '../../genericComponents/loadingComponent/NoDataFoundMessage';
import DialogPizzaComponent from '../dialogComponent/DialogPizzaComponent';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import { useDispatch, useSelector } from 'react-redux';
import { postForm, putForm } from '../../../store/slices/formSlice';
import { AppDispatch, RootState } from '../../../store/store';
import IPizzaFlavor from '../../../interfaces/PizzaFlavor';
import IToppingModel from '../../../interfaces/ToppingModel';
import IAPIResponse from '../../../interfaces/APIResponse';
import IFormData from '../../../interfaces/Form';
import IPizzaState from '../../../interfaces/redux/PizzaState';
import DialogDeleteConfirmation from '../dialogComponent/DialogDeleteConfirmation';
import pizzaActions from '../../../store/actions/pizzaActions';

let deleteFormData: IFormData = {
  formTitle: 'Delete Pizza',
  btnLabelClose: 'Close',
  btnLabelSubmit: 'Delete pizza',
  message: ''
};

const TablePizzaComponent: FC = () => {
  const pizzaState = useSelector<RootState, IPizzaState>((state) => state.pizzas);

  const dispatch = useDispatch<AppDispatch>();
  const [openPizzaDialogModal, setDialogPizzaModal] = useState(false);
  const [openDeleteDialogModal, setDialogDeleteModal] = useState(false);
  const [toppingSelected, setToppingSelected] = useState<IToppingModel[]>([]);
  const [pizzaId, setPizzaId] = useState<string>('');
  const [formConfiguration, setFormConfiguration] = useState<IFormData>(deleteFormData);
  const childRef: any = useRef(null);

  const handleCloseDialogModal = () => {
    setDialogPizzaModal(false);
    setDialogDeleteModal(false);
  };

  const triggerPizzaForm = () => {
    setDialogPizzaModal(true);
  };

  const handleDeletePizza = async () => {
    dispatch(pizzaActions.deletePizza(pizzaId));
    setDialogDeleteModal(false);
  };

  const handleClickOpenPizzaAddForm = () => {
    dispatch(postForm('Pizza'));
    triggerPizzaForm();
  };

  const handleClickOpenPizzaUpdateForm = async (pizza: IPizzaModel) => {
    const pizzaFlavor: IAPIResponse<IPizzaFlavor> = await pizzaServices.getPizzaFlavorById(
      pizza.id
    );
    setToppingSelected(pizzaFlavor.data.toppings);
    triggerPizzaForm();
    dispatch(putForm('Pizza'));
    childRef.current.setValuesToForm(pizzaFlavor.data.pizza, pizzaFlavor.data.toppings);
  };

  const handleClickDeleteDialogConfirmation = async (pizza: IPizzaModel) => {
    deleteFormData.message = `Are you sure that you want to delete the Pizza ${pizza.name} from the store?`;
    setFormConfiguration(deleteFormData);
    setPizzaId(pizza.id);
    setDialogDeleteModal(true);
  };

  return (
    <>
      <Typography variant="h4" className="text-bold">
        Pizza List
        <Button
          className="btn btn-pz-primary"
          onClick={handleClickOpenPizzaAddForm}
          sx={{ marginLeft: 2 }}>
          Add new Pizza
        </Button>
      </Typography>
      <TableContainer sx={{ marginTop: 2 }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell className="text-bold" sx={{ paddingTop: 1, paddingBottom: 1, width: 50 }}>
                Nro
              </TableCell>
              <TableCell className="text-bold" sx={{ paddingTop: 1, paddingBottom: 1 }}>
                {' '}
                Name
              </TableCell>
              <TableCell className="text-bold" sx={{ paddingTop: 1, paddingBottom: 1 }}>
                {' '}
                Description
              </TableCell>
              <TableCell className="text-bold" sx={{ paddingTop: 1, paddingBottom: 1 }}>
                {' '}
                Price
              </TableCell>
              <TableCell className="text-bold" sx={{ paddingTop: 1, paddingBottom: 1 }}>
                {' '}
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          {pizzaState.pizzas.length > 0 ? (
            <TableBody>
              {pizzaState.pizzas.map((pizza: IPizzaModel, index: number) => (
                <TableRow
                  key={index.toString()}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} component="th" scope="row">
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    {pizza.name}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    {pizza.price}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    {pizza.description}
                  </TableCell>
                  <TableCell sx={{ paddingTop: 1, paddingBottom: 1 }} align="left">
                    <ModeEditOutlineOutlinedIcon
                      sx={{ marginLeft: 1, marginRight: 1 }}
                      className="icon-edit"
                      onClick={() => handleClickOpenPizzaUpdateForm(pizza)}
                    />
                    <DeleteOutlinedIcon
                      sx={{ marginLeft: 1, marginRight: 1 }}
                      onClick={() => handleClickDeleteDialogConfirmation(pizza)}
                      className="icon-delete"
                    />
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : (
            <NoDataFoundMessage columnsSize={5} message="No pizzas were found" />
          )}
        </Table>
      </TableContainer>
      <div>
        <DialogPizzaComponent
          closeModal={() => handleCloseDialogModal()}
          isDisplayed={openPizzaDialogModal}
          ref={childRef}
          pizzaToppingSelected={toppingSelected}></DialogPizzaComponent>
        <DialogDeleteConfirmation
          closeModal={() => handleCloseDialogModal()}
          confirmModal={() => handleDeletePizza()}
          isDisplayed={openDeleteDialogModal}
          formConfiguration={formConfiguration}></DialogDeleteConfirmation>
      </div>
    </>
  );
};

export default TablePizzaComponent;
