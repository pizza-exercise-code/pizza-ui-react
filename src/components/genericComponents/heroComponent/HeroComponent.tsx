import { Button, Container, createTheme, Grid, ThemeProvider, Typography } from '@mui/material';
import './HeroComponent.scss';
import { FC } from 'react';
import { useNavigate } from 'react-router';

interface IHeroComponentProps {
  title: string;
  description: string;
  imageUrl?: string;
  button?: {
    buttonLabel: string;
    buttonHref: string;
  };
  componentConfiguration: {
    size: number;
  };
}

const HeroComponent: FC<IHeroComponentProps> = (props) => {
  const { title, imageUrl, description, button, componentConfiguration } = props;
  let navigate = useNavigate();

  const theme = createTheme();
  theme.typography.h2 = {
    fontSize: '1.6rem',
    '@media (min-width:600px)': {
      fontSize: '3rem'
    },
    [theme.breakpoints.up('md')]: {
      fontSize: '4rem'
    }
  };
  theme.typography.body1 = {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'left'
    }
  };

  const redirectTo = (url?: string) => {
    navigate(`${url}`);
  };

  return (
    <Container className="wrapper__hero" sx={{ minHeight: componentConfiguration.size }}>
      <ThemeProvider theme={theme}>
        <Grid container sx={{ m: 'auto' }}>
          {imageUrl ? (
            <Grid container item sm={12} md={6}>
              <img src={imageUrl} alt={title} className="wrapper__hero-image" />
            </Grid>
          ) : (
            <></>
          )}
          <Grid item container sm={12} md={imageUrl ? 6 : 12} sx={{ textAlign: 'left' }}>
            <Grid className="wrapper__hero-information">
              <Typography className="text-bold" variant={'h2'}>
                {title}
              </Typography>
              <Typography variant="body1" textAlign={imageUrl ? 'left' : 'center'}>
                {description}
              </Typography>
              {button ? (
                <Button
                  className="btn btn-pz-primary"
                  sx={{ marginTop: 2 }}
                  onClick={() => redirectTo(button?.buttonHref)}>
                  {button.buttonLabel}
                </Button>
              ) : (
                <></>
              )}
            </Grid>
          </Grid>
        </Grid>
      </ThemeProvider>
    </Container>
  );
};

export default HeroComponent;
