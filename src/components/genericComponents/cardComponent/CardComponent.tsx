import { Button, Grid, Typography } from '@mui/material';
import { FC } from 'react';
import IPizzaModel from '../../../interfaces/PizzaModel';
import './CardComponent.scss';
import { useNavigate } from 'react-router-dom';

const CardComponent: FC<IPizzaModel> = (card) => {
  let navigate = useNavigate();

  const redirectToMoreToppingInformation = (path: string) => {
    navigate(`${path}`);
  };

  return (
    <Grid container className="wrapper__card" key={card.name}>
      <Grid container>
        <img
          src={card.imageUrl}
          alt="pizza"
          className="wrapper__card-image"
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            currentTarget.src =
              'https://ia-latam.com/wp-content/uploads/2018/12/No-image-found-1.jpg';
          }}
        />
      </Grid>
      <Grid container sx={{ marginTop: 2 }}>
        <Typography variant="h6" className="text-bold" sx={{ width: '100%' }}>
          {card.name}
        </Typography>
        <Typography variant="body1" sx={{ width: '100%' }}>
          {card.description}
        </Typography>
        <Typography variant="h6" className="text-bold" sx={{ width: '100%' }}>
          $ {card.price}
        </Typography>
        <Button
          className="btn btn-pz-primary"
          sx={{ marginTop: 2 }}
          onClick={() => redirectToMoreToppingInformation(card.id)}>
          See Toppings
        </Button>
      </Grid>
    </Grid>
  );
};

export default CardComponent;
