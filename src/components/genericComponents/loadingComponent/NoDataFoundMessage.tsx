import { TableBody, TableCell, TableRow, Typography } from '@mui/material';
import { FC } from 'react';

interface NoDataFoundMessageProps {
  columnsSize: number;
  message: string;
}

const NoDataFoundMessage: FC<NoDataFoundMessageProps> = (props) => {
  const { message, columnsSize } = props;

  return (
    <TableBody>
      <TableRow>
        <TableCell colSpan={columnsSize}>
          <Typography align="center" sx={{ width: 'auto' }} variant="body1">
            {message}
          </Typography>
        </TableCell>
      </TableRow>
    </TableBody>
  );
};

export default NoDataFoundMessage;
