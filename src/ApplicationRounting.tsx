import { Routes, Route } from 'react-router-dom';
import Home from './pages/Home/HomePage';
import Store from './pages/Store/StorePage';
import Manage from './pages/Manage/ManagePage';
import PizzaInformation from './pages/PizzaInformation/PizzaInformationPage';
import NotFound from './pages/NotFound/NotFoundPage';

const ApplicationRouting = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/store" element={<Store />} />
      <Route path="/manage" element={<Manage />} />
      <Route path="/store/:id" element={<PizzaInformation />} />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default ApplicationRouting;
