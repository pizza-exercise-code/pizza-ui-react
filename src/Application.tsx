import { BrowserRouter } from 'react-router-dom';
import ApplicationRouting from './ApplicationRounting';
import ApplicationHeader from './components/ApplicationHeader';

const application = () => {
  return (
    <BrowserRouter>
      <ApplicationHeader></ApplicationHeader>
      <ApplicationRouting></ApplicationRouting>
    </BrowserRouter>
  );
};

export default application;
